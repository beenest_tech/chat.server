<?php
namespace App;

class Config
{
  protected static $_config = [
    'hostname'       => 'localhost',
		'port'           => '3306',
		'database'       => 'test_consulpartner',
		'username'       => 'root',
		'password'       => ''
  ];

  public static function get($key)
  {
    return self::$_config[$key];
  }
}