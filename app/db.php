<?php
namespace App;

use \App\Config;

class Db
{
  private $_conn;
  private $_table_name = 'chat_sessions';

  function __construct()
  {
    $host = Config::get('hostname');
    $port = Config::get('port');
    $db   = Config::get('database');
    $username = Config::get('username');
    $password = Config::get('password');

    $this->_conn = new \mysqli($host,$username,$password,$db);
  }

  public function count($args)
  {
    $res = $this->find_user($args);
    return $res->num_rows;
  }

  public function find_user($args)
  {
    $table_name = $this->_table_name;
    $sql_raw = "SELECT * FROM %s WHERE user_id=%d;"; 
    $sql     = sprintf($sql_raw,$table_name,$args['user_id']);
    
    $res = $this->_conn->query($sql);
    return $res;
  }

  public function get_connection_id($args)
  {
    $res = $this->find_user($args);

    if($res->num_rows > 0)
    {
      while($row = $res->fetch_assoc())
        return $row['connection_id'];
    }
    return null;
  }

  public function insert_connection($args)
  {
    $table_name = $this->_table_name;

    if($this->count($args) >= 1){
      $sql_raw = "UPDATE $table_name SET connection_id=%d WHERE user_id=%d;";
      $sql = sprintf($sql_raw,$args['connection_id'],$args['user_id']);
    } else {
      $sql_raw = "INSERT INTO $table_name (user_id,connection_id) VALUES (%d,%d);";
      $sql = sprintf($sql_raw,$args['user_id'],$args['connection_id']);
    }

    return $this->_query($sql);
  }

  public function remove_connection($resource_id)
  {
    $table_name = $this->_table_name;
    
    $sql_raw = "UPDATE $table_name SET connection_id=NULL WHERE connection_id=%d";
    $sql     = sprintf($sql_raw,$resource_id);

    return $this->_query($sql);
  }

  //Run this to clean the chat session when the WebSocket server restarts
  //Purges the remaining connected users
  public function clean_connections()
  {
    $table_name = $this->_table_name;

    $sql = "UPDATE $table_name SET connection_id=NULL";

    return $this->_query($sql);
  }

  private function _query($sql)
  {
    if($this->_conn->query($sql) === TRUE){
      return true;
    } else {
      echo "Error on SQL ".$sql." ".$this->_conn->error;
    }
  }
}