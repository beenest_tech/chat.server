<?php
namespace Chat;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use App\Db;

class Component Implements MessageComponentInterface
{
  protected $_clients;

  function __construct()
  {
    //prepare the container to store client information
    $this->_clients = new \SplObjectStorage;
    
    $db = new Db();
    $db->clean_connections();
  }

  public function onOpen(ConnectionInterface $conn)
  {
    //attach newly connected clients
    $this->_clients->attach($conn);
    
    //Fetch the user_id attached in the ws connection
    $queryString = $conn->WebSocket->request->getQuery()->toArray();
    $args['user_id']        = $queryString['user_id'];
    //get the resource id
    $args['connection_id']  = $conn->resourceId;
    //Insert / Update the chat session DB
    $db = new Db();
    $db->insert_connection($args);
    
    echo "New Connection Established ($conn->resourceId) \n";
  }

  public function onMessage(ConnectionInterface $from,$payload)
  {
    //convert the recieved payload to array string
    $msg = json_decode($payload,true);
    //the sender unique ID
    $from_id = $from->resourceId;
    $to_id   = null;
    //the reciever unique ID
    //If no reciever sent, broadcast to everyone except user
    if(isset($msg['params']['id']))
    {
      $db = new Db();
      $args['user_id'] = $msg['params']['to_id'];
      //get the connection id
      $to_id           = $db->get_connection_id($args);
    }
    
    //project_id
    $prj_id  = $msg['params']['prj_id'];

    foreach($this->_clients as $client)
    {
      $client_id = $client->resourceId;
      if($from_id != $client_id){
          $client->send($payload);
      }
    }
  }

  public function onClose(ConnectionInterface $conn)
  {
    //Remove disconnected client
    $this->_clients->detach($conn);
    //Remove resource id in chat session table
    $db = new Db();
    $db->remove_connection($conn->resourceId);
  }

  public function onError(ConnectionInterface $conn, \Exception $e)
  {
    //display error when it happens
    echo "An error occured: { $e->getMessage() } \n";
    $conn->close();
  }
}
