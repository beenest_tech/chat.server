# Chat Server for Consul Partners

##Description

This is a websocket server to enable push notification for the x602 templates. Without this, new messages won't appear instantaneously on each client.

##Preparation

1. Add the `chat_sessions.sql` table to your `consulpartner table`.
2. Go to `app/config.php` and change the information in the `$_config` property to match the database details for console parters.
3. In the command line. Type `php composer.phar install` to install the third party libraries.
4. run this on the command line `php server.php` if it shows like [this](http://prnt.sc/cx11b5), it means that the server is working.
5. Go to the X602 templates to test. Messages should be recived instantly. In the server command line there should be an [indicator](http://prnt.sc/cx12hg) that the client connected without problems to the server.

##Reminders

* This websocket server need to run for the push notifcation to work.
* To test in the staging. **You need run the script and keep the ssh console running, or else it won't work.**

##What needed to do

* Kindly find a way to run this script **as a service/deamon** in CentOS in order to keep the push notification working without having to keep the ssh console open.
