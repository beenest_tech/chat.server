<?php
require("vendor/autoload.php");

use Ratchet \Server\IoServer;
use Chat\Component;

//For use in front-end app
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

//Web Socket Class
$ws   = new WsServer(new Component());
//Pass WS class to Httpserver class
$http = new HttpServer($ws);

$server = IoServer::factory(
  $http,
  8100
);

$server->run();